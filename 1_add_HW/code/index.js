async function getServer(url) {
    try{
    const info = await fetch(url);
    
    const usersArr = await info.json();
    
    const myArr = usersArr.map(({id, name, email, phone})=>{
        return{
            "id": id,
            "name": name,
            "email": email,
            "phone": phone,
        }
    });
   
    show(myArr);
    }catch(e){
        alert(e)
        console.error(e);
    }
}

async function show(data = []) {
    data.forEach(({id, name, email, phone})=>{

        const info =` <tr">
                    <td><h4>${id}</h4></td>
                    <td><h4>${name}</h4></td>
                    <td><h4>${email}</h4></td>
                    <td><h4>${phone}</h4></td>
                </tr>`

        document.querySelector("tbody")
            .insertAdjacentHTML("beforeend", info);                
        });


}

getServer("https://jsonplaceholder.typicode.com/users");
