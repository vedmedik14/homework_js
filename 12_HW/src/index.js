/*
* У папці calculator дана верстка макета калькулятора. 
Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/

const keys = document.querySelector(".keys");

const textArea = document.querySelector(".textArea");
const orange = document.querySelector(".orange"); 
const pink = document.querySelectorAll(".pink"); 
const indikator = document.querySelector(".indikator"); 
const gray = document.querySelectorAll(".gray"); 
const mPlus = document.querySelector(".mPlus"); 
const mrc = document.querySelector(".mrc"); 

pink.forEach(function (el) {
    el.setAttribute("disabled","true");
});
gray.forEach(function (el) {
    el.setAttribute("disabled","true");
});

let num1="",
num2="",
saveValue = "",

flag = true,
pointFlag = true,

multiFlag = false,
mFlag = false;
diviFlag = false,
plusFlag = false,
minusFlag = false;
pattern = /\d/;

keys.addEventListener("click", function () {

    if (saveValue !=0 || saveValue !="") {
        gray.forEach(function (el) {
            el.removeAttribute("disabled");
        });
        console.log(saveValue);
    }

//M start
    if (event.target.value == "mrc") {
        if (mFlag==true) {
            if(flag == true){
                num1 = `${saveValue}`;
            }else{
                num2 = `${saveValue}`;
            }
            mFlag = false;
            textArea.value = saveValue;    
            gray.forEach(function (el) {
                el.setAttribute("disabled", "true");
            });
        }else{
            saveValue = "";
            indikator.style.zIndex = "-1";
            gray.forEach(function (el) {
                el.setAttribute("disabled", "true");
            });
            mPlus.removeAttribute("disabled");
        }
    }

    if (event.target.value == "m+") {
        if (saveValue!=textArea.value) {
            saveValue = textArea.value;
            indikator.style.zIndex = "1";
            mFlag=true;
            mPlus.setAttribute("disabled","true");
        }
    }

    if (event.target.value == "m-") {
        saveValue = "";
        gray.forEach(function (el) {
            el.setAttribute("disabled", "true");
        });
        mPlus.removeAttribute("disabled");
        indikator.style.zIndex = "-1";
    }
//////////////////////////////////////////////

// C start
    if (event.target.value == "C") {

        pink.forEach(function (el) {
            el.setAttribute("disabled","true");
        });
        gray.forEach(function (el) {
            el.setAttribute("disabled","true");
        });

        indikator.style.zIndex = "-1";

        textArea.value = '';
        num1="",
        num2="",
        saveValue = "",

        mFlag=false;
        flag = true,
        pointFlag = true,

        multiFlag = false,
        diviFlag = false,
        plusFlag = false,
        minusFlag = false;
    }
///////////////////////////////////////////////


// 0-9 start
    if (pattern.test(event.target.value)){
        debugger;
        if(flag == true){
            num1 = `${num1}${event.target.value}`;
            textArea.value = num1;
        }else{
            num2 = `${num2}${event.target.value}`;
            textArea.value = num2;
        }
        pink.forEach(function (el) {
            el.removeAttribute("disabled");
        });
        mPlus.removeAttribute("disabled");
        mrc.setAttribute("disabled","true");
    }
////////////////////////////////////////////////

// point start 
    if (event.target.value == ".") {
        symValue = ".";
        if (pointFlag == true) {

            if(flag == true){
                num1 = `${num1}.`;
                textArea.value = num1;

                pointFlag = false;
            }else{
                num2 = `${num2}.`;
                textArea.value = num2;

                pointFlag = false;
            }
        }
    }
/////////////////////////////////////////////////

// division start
    if (event.target.value == "/") {
        orange.removeAttribute("disabled");
        mrc.removeAttribute("disabled","true");
        textArea.value = "";

        diviFlag = true;
        pointFlag = true;
        flag = false;
        mFlag=true;
    }
///////////

//multipl. start
    if (event.target.value == "*") {
        orange.removeAttribute("disabled");
        mrc.removeAttribute("disabled","true");
        textArea.value = "";

        multiFlag = true;
        pointFlag = true;
        flag = false;
        mFlag=true;
    }
//////////

//plus start
    if (event.target.value == "+") {
        orange.removeAttribute("disabled");
        mrc.removeAttribute("disabled","true");
        textArea.value = "";

        plusFlag = true;
        pointFlag = true;
        flag = false;
        mFlag=true;
    }
///////////

//minus start
    if (event.target.value == "-") {
        orange.removeAttribute("disabled");
        mrc.removeAttribute("disabled","true");
        textArea.value = "";

        minusFlag = true;
        pointFlag = true;
        flag = false;
        mFlag=true;
        
    }
///////////

    let num1Pars, num2Pars, res;

        if (event.target.value == "=") {           
            
            if (diviFlag == true) {
                num1Pars = parseFloat(num1);
                num2Pars = parseFloat(num2);

                if(num2Pars != 0){
                res = num1Pars/num2Pars;
                console.log(res);

                diviFlag = false;
                orange.setAttribute("disabled", "true");
            }else{
                textArea.value = "not possible";
            }
        }

        if (multiFlag == true) {
            num1Pars = parseFloat(num1);
            num2Pars = parseFloat(num2);

            res = num1Pars*num2Pars;
            console.log(res);

            multiFlag = false;
            orange.setAttribute("disabled", "true");
        }

        if (plusFlag == true) {
            num1Pars = parseFloat(num1);
            num2Pars = parseFloat(num2);

            res = num1Pars+num2Pars;
            console.log(res);

            plusFlag = false;
            orange.setAttribute("disabled", "true");
        }

        if (minusFlag == true) {
            num1Pars = parseFloat(num1);
            num2Pars = parseFloat(num2);

            res = num1Pars-num2Pars;
            console.log(res);

            minusFlag = false;
            orange.setAttribute("disabled", "true");
        }

        

        if (num1 != undefined || num1 != null ||num1 != "") {
            flag = false;
            mFlag=true;
            multiFlag = false,
            diviFlag = false,
            plusFlag = false,
            minusFlag = false;

            num1 = res.toString();
            num2 = "";
        }

        textArea.value = res;
        mrc.setAttribute("disabled","true");
    }

})