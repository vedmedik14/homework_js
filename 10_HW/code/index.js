
const second = document.querySelector("#second");
const minute = document.querySelector("#minute");
const hour = document.querySelector("#hour");

const startBt = document.querySelector("#start");
const stopBt = document.querySelector("#stop");
const resetBt = document.querySelector("#reset");

const container = document.querySelector(".container-stopwatch");

let flag = true,
secCounter = 0,
minCounter = 0,
hourCounter =98,
interval;


const count = function () {
    if (hourCounter==99) {
        clearInterval(interval);
        console.log("end");
    }else{
        if (secCounter<10){
            second.innerHTML = "0" + secCounter;
            secCounter++;
        }else{
            second.innerHTML = secCounter;
            secCounter++;
        }

        if (secCounter == 59) {
            secCounter = 0;
            if (minCounter<9) {
                minCounter++;
                minute.innerHTML = "0" + minCounter;
            }else{
                minCounter++;
                minute.innerHTML = minCounter;
            }
        }

        if (minCounter == 59) {
            minCounter = 0;
            if (hourCounter<9){
                hourCounter++;
                hour.innerHTML = "0" + hourCounter;
            }else{
                hourCounter++;
                hour.innerHTML = hourCounter;
            }
        }
    }
}

startBt.onclick=()=>{
    container.classList.add("green");
    container.classList.remove("red");
    container.classList.remove("black");
    if(flag == true){
        interval = setInterval(count,1000);
    }
    flag = false;
}

stopBt.onclick=()=>{
    if (flag == false) {
        container.classList.add("red");
        container.classList.remove("green");
        flag = true;
        clearInterval(interval);
    }
}

resetBt.onclick=()=>{
    if (flag == true) {
        secCounter = 0;
        minCounter = 0;
        hourCounter = 0;

        second.innerHTML ="00";
        minute.innerHTML ="00";
        hour.innerHTML ="00";
    }
    container.classList.remove("red");
    container.classList.add("black");
}

// // // // // // // // // // // // // // // // // // // // //

const numersArr=[];

const body = document.querySelector("body");

const numInp = document.createElement("input");
numInp.classList.add("numInp");
numInp.setAttribute("type","tel");
numInp.setAttribute("placeholder","380-000-00-00");

const btSaveInp = document.createElement("input");
btSaveInp.classList.add("btSaveInp");
btSaveInp.setAttribute("type","button");
btSaveInp.setAttribute("value","SAVE NUMBER");

const er = document.createElement("div");
er.classList.add("er");
er.style.backgroundColor = "rgba(181, 59, 59, 0.5)";
er.innerText ="not correct number";

let pattern = /380\-\d{3}\-\d{2}\-\d{2}/;

btSaveInp.onclick=()=>{
    if (pattern.test(numInp.value)) {
        numInp.style.backgroundColor="rgba(0, 150, 0, 0.5)";
        numersArr.push(numInp.value);
        alert("number saved!");
        window.location.href = "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg";
    }else{
        numInp.style.backgroundColor="rgba(181, 59, 59, 0.5)";
        console.error("not correct number");
        numInp.before(er);
    }
}

body.insertAdjacentElement("afterbegin",numInp);
numInp.after(btSaveInp);


// 000-000-00-00