
/*class Worker{
    constructor(name,surname,rate,days){
        this.name = name;
        this.surname = surname;
        this.rate = rate;
        this.days = days;  
    }
    

}
Worker.prototype.getSalary = function(){
    console.log(this.days*this.rate);
}

const worker1 = new Worker('Ivan','Zax',15,22);
worker1.getSalary();



// // // // // // // // MyString
class MyString{
    constructor(str){
        this.str = str;
    }
   
}
MyString.prototype.rever = function(){
    let resStr = "";
    for (let i = this.str.length-1; i > -1; i--) {
        resStr += this.str[i];
    }
}
MyString.prototype.usFirst= function(){
    return this.str.replace(this.str.charAt(0),this.str.charAt(0).toUpperCase());
}
MyString.prototype.usWords=function(){
    let res = new Array;
    let arr = this.str.split(" ");
    for (let i = 0; i < arr.length; i++) {
        res[i] = arr[i].replace(arr[i].charAt(0),arr[i].charAt(0).toUpperCase());
    }
    const str = res.join(" ");
}

const str1 =new MyString ('hello');

str1.rever();
str1.usFirst();
str1.usWords();


// // // // // // // // Phone

class Phone {
    constructor(number, model, weight){
        this.number = number;
        this.model = model;
        this.weight = weight;
    }
    
}
Phone.prototype.receiveCall=function(name){
    console.log(`Телефонує ${name}`);
}
Phone.prototype.getNumber=function(number){
    console.log(number);
}

const samsung = new Phone(4455,'samsung',200);
const iphone = new Phone(8899,'iphone',180);
const onePlus = new Phone(1122,'onePlus',158);

samsung.receiveCall('Tim');
samsung.getNumber(`+49097908675`);

samsung.receiveCall('Niko');
samsung.getNumber(`+49098938675`);

samsung.receiveCall('Adrian');
samsung.getNumber(`+49099558675`);


// // // // // // // // Driver

class Driver{
    constructor(ПІБ,стаж){
        this.ПІБ=ПІБ;
        this.стаж=стаж;
    }
}
class Engine{
    constructor(потужність, виробник){
        this.потужність=потужність;
        this.виробник=виробник;
    }
}
class Car{
    constructor(марка,клас,вага,водій,мотор){
        this.марка=марка;
        this.клас=клас;
        this.вага=вага;
        this.водій=водій;
        this.мотор=мотор;
    }
    
}
Car.prototype.start=function(){
    console.log("Поїхали");
}
Car.prototype.stop=function(){
    console.log("Зупиняємося");
}
Car.prototype.turnRight=function(){
    console.log("Поворот праворуч");
}
Car.prototype.turnLeft=function(){
    console.log("Поворот ліворуч");
}
Car.prototype.toString=function(){
    console.log(`
марка: ${this.марка},
клас: ${this.клас},
вага: ${this.вага},
водій: ${this.водій.ПІБ}, 
стаж: ${this.водій.стаж},
двигун: ${this.мотор.потужність}, 
виробник: ${this.мотор.виробник},
max вантаж: ${this.capacity},
max швидкість: ${this.maxSpeed}`);
}
class Lorry extends Car{
    constructor(марка, клас, вага,водій,мотор,capacity){
        super(марка,клас,вага,водій,мотор);
        this.capacity=capacity;
    }
}
class SportCar extends Car{
    constructor(марка, клас, вага,водій,мотор,maxSpeed){
        super(марка,клас,вага,водій,мотор);
        this.maxSpeed=maxSpeed;
    }
}

const tesla = new Driver("Ivan Ivanov Ivanovich",5);
const v8 =new Engine("440лс", 'DniproMoto');
const frend =new Car("tesla","C1","1200кг",tesla,v8);
frend.toString();

const MAN = new Lorry ("MAN", "C1", "5000",tesla,v8,"12000")
MAN.toString()

const F1 = new SportCar("Mustang","B1","1000", tesla, v8 ,"400km/h")
F1.toString();
*/
// // // // // // // // Animal

class Animal{
    constructor( food, location){
        this.food=food;
        this.location=location;
    }
}
Animal.prototype.makeNoise=function(){
    console.log("може говорити");
} 
Animal.prototype.eat=function(){
    console.log("може їсти");
} 
Animal.prototype.sleep=function(){
    console.log("може спати");
}
class Dog extends Animal{
    constructor(food,location,sit){
        super(food, location);
        this.sit=sit;
    }
}
Dog.prototype.makeNoise=function(){
    console.log("гав");
} 
Dog.prototype.eat=function(){
    console.log("хижак");
} 
class Cat extends Animal{
    constructor(food,location,tocacth){
        super(food, location);
        this.tocacth=tocacth;
    }
   
}
Cat.prototype.makeNoise=function(){
    console.log("няв");
} 
Cat.prototype.eat=function(){
    console.log("хижак");
} 
class Horse extends Animal{
    constructor(food,location,gallop){
        super(food, location);
        this.gallop=gallop;
    }   
}
Horse.prototype.makeNoise=function(){
    console.log("ігого");
} 
Horse.prototype.eat=function(){
    console.log("травоядне");
} 

class Vet{
    constructor(food,location){
        this.location=location;
        this.food=food;
    }
}

Vet.prototype.main=function(){
    const allAnimals= new Animal;
    allAnimals=[Dog,Cat,Horse];

    for (let i = 0; i < allAnimals.length; i++) {
        `${allAnimals[i]} go to veterinar`
    }
}

Vet.prototype.treatAnimal=function(){
    console.log(`
    food: ${this.food} 
    location: ${this.location}`);
}

const mustang =new Horse("grass","America",60)

const hor = new Vet(mustang.food, mustang.location);
hor.treatAnimal();