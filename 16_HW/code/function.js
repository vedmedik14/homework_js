
const doneArr = [];

export const req = async (url) => {
    document.querySelector(".box_loader").classList.add("show")
    const data = await fetch(url);
    return await data.json();
}

export function show(data = []) {
    if (!Array.isArray(data)) return ;
    const tbody = document.querySelector("tbody");
  tbody.innerHTML = ""
    //HW
    const newArr = data.map(({txt, rate, exchangedate, title, completed, name, diameter, climate}, i) => {
        
        return {
            id: i + 1,
            name : txt || title || name,
            info1 : rate || completed || `${diameter} km`,
            info2 : exchangedate || climate || "тут пусто"
        }
    //
    });

    newArr.forEach(({ name, id, info1, info2 }) => {
        tbody.insertAdjacentHTML("beforeend", `
        <tr> 
        <td>${id}</td>
        <td>${name}</td>
        <td>${info1}</td>
        <td>${info2}</td>
        </tr>
        `)
    })

    document.querySelector(".box_loader").classList.remove("show")
}

//HW
export function nextPage(info) {
    
         info.results.forEach(({ name, diameter, climate})=>{
            doneArr.push({
                "name": name,
                "diameter": diameter,
                "climate": climate
            })
        });
    if (info.next ==null || info.next == undefined) {
        show(doneArr);
    }else{
       req(info.next)
       .then((link)=>{
            nextPage(link);         
       });
    }
}
//