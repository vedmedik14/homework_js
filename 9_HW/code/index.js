

window.onload=()=>{

    const body = document.querySelector("body");
    body.innerText = "body";

//header1
    const header1 = document.createElement("header");
    header1.classList.add("header1");

    header1.textContent = "HEADER";
    header1.style.backgroundColor = "rgba(185, 191, 196,0.5)";
//

//nav1
    const nav1 = document.createElement("nav");
    nav1.classList.add("nav1");

    nav1.innerText = "NAV";
    nav1.style.backgroundColor = "rgba(31, 31, 216, 0.3)";
// 

//section
    const section= document.createElement("section");
    section.classList.add("section");
// 

    //sectionLeft
        const sectionLeft= document.createElement("section");
        sectionLeft.classList.add("sectionLeft");

        sectionLeft.innerText = "SECTION";
        sectionLeft.style.backgroundColor = "rgba(185, 191, 196,0.5)";
    // 

        //headerLeft 
            const headerLeft = document.createElement("header");
            headerLeft.classList.add("headerLeft");

            headerLeft.innerText = "HEADER";
            headerLeft.style.backgroundColor = "rgba(31, 31, 216, 0.3)";
            headerLeft.style.height = "8%";
            headerLeft.style.width = "90%";
        //

        //article1 
            const article1 = document.createElement("article");
            article1.classList.add("article1");

            article1.innerText = "ARTICLE";
            article1.style.backgroundColor = "rgba(31, 31, 216, 0.3)";
            article1.style.height = "40%";
            article1.style.width = "90%";
            
        // 

            //arHeader
                const arHeader = document.createElement("header");
                arHeader.classList.add("arHeader");
                arHeader.classList.add("ar1");
        
                arHeader.innerText = "HEADER";
            // 
                
            // arP
                const arP = document.createElement("p");
                arP.classList.add("arP");
                arP.classList.add("ar1");
        
                arP.innerText = "P";
            // 

            //arInContainer
                const arInContainer = document.createElement("div");
                arInContainer.classList.add("arInContainer");
            // 

                //arPIn 
                    const arPIn = document.createElement("p");
                    arPIn.classList.add("arPIn");
                    arPIn.classList.add("arIn");
            
                    arPIn.innerText = "P";
                // 

                //aside 
                    const aside = document.createElement("aside");
                    aside.classList.add("aside");
                    aside.classList.add("arIn");
            
                    aside.innerText = "ASIDE";
                // 

            //arFooter 
                const arFooter = document.createElement("footer");
                arFooter.classList.add("arFooter");
                arFooter.classList.add("ar1");

                arFooter.innerText = "FOOTER";
            // 

        //article2 
            const article2 = document.createElement("article");
            article2.classList.add("article2");

            article2.innerText = "ARTICLE";
            article2.style.backgroundColor = "rgba(31, 31, 216, 0.3)";
            article2.style.height = "40%";
            article2.style.width = "90%";
        // 

            //ar2Header
                const ar2Header = document.createElement("header");
                ar2Header.classList.add("ar2Header");
                ar2Header.classList.add("ar1");
        
                ar2Header.innerText = "HEADER";
            // 
            
            //ar2P
                const ar2P = document.createElement("p");
                ar2P.classList.add("ar2P");
                ar2P.classList.add("ar1");
        
                ar2P.innerText = "P";
            // 

            //ar2P2
                const ar2P2 = document.createElement("p");
                ar2P2.classList.add("ar2P2");
                ar2P2.classList.add("ar1");
        
                ar2P2.innerText = "P";
            // 

            //ar2Footer 
                const ar2Footer = document.createElement("footer");
                ar2Footer.classList.add("ar2Footer");
                ar2Footer.classList.add("ar1");

                ar2Footer.innerText = "FOOTER";
            // 

        //sectionFooter 
            const sectionFooter = document.createElement("footer");
            sectionFooter.classList.add("sectionFooter");

            sectionFooter.innerText = "FOOTER";
            sectionFooter.style.backgroundColor = "rgba(31, 31, 216, 0.3)";
            sectionFooter.style.height = "8%";
            sectionFooter.style.width = "90%";
        // 



    //sectionRight 
        const sectionRight= document.createElement("section");
        sectionRight.classList.add("sectionRight");

        sectionRight.innerText = "SECTION";
        sectionRight.style.backgroundColor = "rgba(185, 191, 196,0.5)";
    // 

        //headerRight 
            const headerRight = document.createElement("header");
            headerRight.classList.add("headerRight");

            headerRight.innerText = "HEADER";
            headerRight.style.backgroundColor = "rgba(31, 31, 216, 0.3)";
            headerRight.style.height = "7%";
            headerRight.style.width = "90%";
        // 

        //navRight
            const navRight = document.createElement("nav");
            navRight.classList.add("navRight");

            navRight.innerText = "NAV";
            navRight.style.backgroundColor = "rgba(31, 31, 216, 0.3)";
            navRight.style.width = "90%";
        // 

    //lastFooter 
        const lastFooter = document.createElement("footer");
        lastFooter.classList.add("lastFooter");

        lastFooter.innerText = "FOOTER";
    // 


//  
    body.insertAdjacentElement("afterbegin",header1);
        header1.insertAdjacentElement("beforeend",nav1);
    
        header1.after(section);
            section.insertAdjacentElement("afterbegin",sectionRight);
                sectionRight.insertAdjacentElement("beforeend",headerRight);
                headerRight.after(navRight);

            section.insertAdjacentElement("afterbegin",sectionLeft);
                sectionLeft.insertAdjacentElement("beforeend",headerLeft);
                headerLeft.after(article1);
                article1.after(article2);
                    article1.insertAdjacentElement("beforeend",arHeader);
                    arHeader.after(arP);
                    arP.after(arInContainer);
                        arInContainer.insertAdjacentElement("beforeend",arPIn);
                        arPIn.after(aside);
                    arInContainer.after(arFooter);
                    
                article2.after(sectionFooter);
                    article2.insertAdjacentElement("beforeend",ar2Header);
                    ar2Header.after(ar2P);
                    ar2P.after(ar2P2);
                    ar2P2.after(ar2Footer);
        section.after(lastFooter);

    body.addEventListener("click",function(event){
        let newText = prompt("write your text here:",`${event.target.innerText}`);
        event.target.childNodes[0].textContent = `${newText}`
    });
}
