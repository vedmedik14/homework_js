
// 1 , 2

const validate = (p, v) => p.test(v);

const inputs = document.querySelectorAll(".input");
const p = document.querySelector("#test");
    
inputs.forEach((input)=>{
    input.addEventListener("blur", (e) => {

     if(e.target.dataset.type === "name" && validate(/^[а-яієґї'-]{2,}$/ig, e.target.value)){
            e.target.classList.add("valid")
            p.innerText = e.target.value; 
        }else if (e.target.dataset.type === "surname" && validate(/^[а-яієґї'-]{2,}$/ig, e.target.value)){
            e.target.classList.add("valid");
            p.innerText = e.target.value; 
        }else if(e.target.dataset.type === "phone" && validate(/^\+380\d{2} \d{3} \d{2} \d{2}$/ig, e.target.value)){
            e.target.classList.add("valid");
            p.innerText = e.target.value; 
        }else if(e.target.dataset.type === "email" && validate(/^[a-z_.0-9]+@[a-z0-9-.]+\.[a-z.]+$/ig, e.target.value)){
            e.target.classList.add("valid");
            p.innerText = e.target.value; 
        }else if(e.target.dataset.type === "city"){
            p.innerText = e.target.value; 
        }else{
            e.target.className = ""
            e.target.classList.add("error");
        }
});

// 4

const container = document.querySelector(".spanContainer");
const span = document.createElement("span");

const xButton = document.createElement("input");
xButton.setAttribute("type","button");
xButton.setAttribute("value","X");

const erDiv = document.createElement("div");
erDiv.classList.add("erDiv");

const inp = document.querySelector("#inp");

let inpValue = "";

inp.addEventListener("focus", (e) => {
    e.target.classList.add("inpValid");
});
inp.addEventListener("blur", (e) => {
    if (e.target.value>0) {
        container.insertAdjacentElement("afterbegin", span);
        span.after(xButton);

        container.classList.add("border");
        inp.classList.add("inpBack");

        inpValue = e.target.value;
        span.innerText = inpValue;

        erDiv.innerText = "";
    }
    else{
        inp.after(erDiv);
        erDiv.innerText = "Please enter correct price";
    }
});
xButton.addEventListener("click", (e) => {
    container.removeChild(span,);
    container.removeChild(xButton);

    container.classList.remove("border");
    inp.classList.remove("inpBack");
});
});