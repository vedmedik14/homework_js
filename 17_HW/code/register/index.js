import {registerTester} from "../validator.js"
import { creat } from "../functions.js";


const body = document.querySelector("body");

function creatInputs() {    

    const inputsContainer = document.querySelector(".inputsContainer");
    const btContainer = document.querySelector(".btContainer");

    const nameInp = creat("nameInp", "Name", "text", "Niko");
    inputsContainer.insertAdjacentHTML("beforeend",nameInp);

    const brthInp = creat("brthInp", "Date Of Birth", "date", "00.00.0000");
    inputsContainer.insertAdjacentHTML("beforeend",brthInp);

    const patronName = creat("patronName", "Father’s/Mother’s Name", "text", "David");
    inputsContainer.insertAdjacentHTML("beforeend",patronName);

    const emailInp = creat("emailInp", "Email", "email", "name@gmail.com");
    inputsContainer.insertAdjacentHTML("beforeend",emailInp);

    const tel = creat("tel", "Mobile Nomber", "tel", "+380979973232");
    inputsContainer.insertAdjacentHTML("beforeend",tel);

    const passwordInp = creat("passwordInp", "Password", "password", "8-12 sumvols wihout ^-() /");
    inputsContainer.insertAdjacentHTML("beforeend",passwordInp);

    const rePasswordInp = creat("rePasswordInp", "Re-enter Password", "password", "•••••••••");
    inputsContainer.insertAdjacentHTML("beforeend",rePasswordInp);

    const streetInp = creat("streetInp", "Street", "text", "homeStr.");
    inputsContainer.insertAdjacentHTML("beforeend",streetInp);


    const submitBt = document.createElement("input");
        submitBt.classList.add("submit")
        submitBt.setAttribute("type","button");
        submitBt.setAttribute("value","submit");
    btContainer.insertAdjacentElement("beforeend",submitBt);

    registerTester();

}

function creatContainer() {
    const mainContainer =`
    <div class="leftContainer">
        <img src="../assets/imgs/Figure.png" alt="logo">
    </div>
    <div class="rightContainer">
        <div class="logo">
            <h2>Registration form</h2>
        </div>
        <div class="inputsContainer"></div>
        <div class="btContainer"></div>
    </div>`
    body.insertAdjacentHTML("afterbegin",mainContainer);   
    creatInputs();                                                                                                                                                                                                                                              
}

body.addEventListener("DOMContentLoaded", creatContainer());