import { validator, checkUser,
        checkName, checkPatronName,
        checkEmail, checkTel,
        checkPassword, checkRepassword,
        checkStreet, checkSubmit} from "./functions.js";

export function mainTester() {
    const loginBt = document.querySelector(".loginBt");

    loginBt.addEventListener("click", checkUser);
}


export function registerTester() {

    document.querySelector("#nameInp")
    .addEventListener("change",checkName);

    document.querySelector("#patronName")
    .addEventListener("change",checkPatronName);

    document.querySelector("#emailInp")
    .addEventListener("change",checkEmail);

    document.querySelector("#tel")
    .addEventListener("change",checkTel);

    document.querySelector("#passwordInp")
    .addEventListener("change",checkPassword);

    document.querySelector("#rePasswordInp")
    .addEventListener("change",checkRepassword);

    document.querySelector("#streetInp")
    .addEventListener("change",checkStreet);


    document.querySelector(".submit")
    .addEventListener("click",checkSubmit);
}

