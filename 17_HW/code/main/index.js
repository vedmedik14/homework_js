import {mainTester} from "../validator.js"
// import { creatLoginContainer } from "../functions.js";

const body = document.querySelector("body");                                     

function creatInputs() {                                                                                                                      

    const inputsContainer = document.querySelector(".inputsContainer");
    const btContainer = document.querySelector(".btContainer");

    const loginLable = document.createElement("lable");
        loginLable.innerText = "Login";
        loginLable.classList.add("loginLable");

    const loginInp = document.createElement("input");
        loginInp.classList.add("loginInp");
        loginInp.setAttribute("type", "email");
        loginInp.setAttribute("placeholder", "name@gmail.com");


    const passLable = document.createElement("label");
        passLable.classList.add("passLable");
        passLable.innerText = "Password";

    const paswordInp = document.createElement("input");
        paswordInp.classList.add("paswordInp");
        paswordInp.setAttribute("type", "password");
        paswordInp.setAttribute("placeholder", "Password");
    

    inputsContainer.insertAdjacentElement("beforeend",loginLable);
    loginLable.after(loginInp);
    inputsContainer.insertAdjacentElement("beforeend",passLable);
    passLable.after(paswordInp);

    const loginBt = document.createElement("input");
        loginBt.classList.add("loginBt");
        loginBt.setAttribute("type", "button");
        loginBt.setAttribute("value", "Login");

    btContainer.insertAdjacentElement("afterbegin", loginBt);

    mainTester();
}

function creatLoginContainer() {
    const mainContainer =`
    <div class="leftContainer">
        <img src="./assets/imgs/login 1.png" alt="logo">
    </div>
    <div class="rightContainer">
        <div class="logo">
            <h2>Login</h2>
        </div>
        <div class="inputsContainer"></div>
        <div class="linkContainer">
            Not a user?<a href="./Register/index.html"> Register now</a>
        </div>
        <div class="btContainer"></div>
    </div>`
    body.insertAdjacentHTML("afterbegin",mainContainer);

    creatInputs();
}
creatLoginContainer();
