/*
// 1
const arr1 = ['a','b','c'];
const arr2 = [1, 2, 3];

const res = arr1.concat(arr2);

document.write(`${arr1}</br>`);
document.write(`${arr2}</br>`);
C

*/


/*
//2
const arr1 = ['a','b','c'];

arr1.push(1,2,3);

document.write(`${arr1}</br>`);

*/


/*
//3

const arr1 = [1, 2, 3];

arr1.reverse();

document.write(`${arr1}</br>`);

*/


/*
//4

const arr1 = [1,2,3];

arr1.push(4,5,6);

document.write(`${arr1}</br>`);

*/

/*
//5
const arr1 = [1,2,3];

arr1.unshift(4,5,6);

document.write(`${arr1}</br>`);

*/

/*
//6
const arr1 = ['js', 'css', 'jq'];

document.write(`${arr1[0]}</br>`);

*/

/*
//7
const arr1 = [1,2,3,4,5];

arr1.push(arr1.slice(0,3));

document.write(`${arr1}</br>`);

*/

/*
//8
const arr1 = [1,2,3,4,5];

arr1.splice(1,2);

document.write(`${arr1}</br>`);

*/

/*
//9
const arr1 = [1,2,3,4,5];

arr1.splice(2,0,10);

document.write(`${arr1}</br>`);

*/

/*
//10
const arr2 = [3, 4, 1, 2, 7];

arr2.sort();

document.write(`${arr2}</br>`);

*/

/*
//11
const arr2 = ['Привіт,', 'світ', '!'];

arr2.splice(1,1,'мир');

document.write(`${arr2.join(' ')}</br>`);

*/

/*
//12
const arr2 = ['Привіт,', 'світ', '!'];

arr2[0]='Поки,'

document.write(`${arr2.join(' ')}</br>`);

*/

/*
//13
const arr1 = [1,2,3,4,5];
document.write(`${arr1}</br>`);


const arr2 = new Array(5)

for (let i = 0; i < arr2.length; i++) {
    arr2[i]=i+1;
}
document.write(`${arr2}</br>`);

*/

/*
//14
var arr = {
    'ua':['блакитний', 'червоний', 'зелений'],
    'en':['blue', 'red', 'green'],
};
document.write(`${arr['ua'][0]} ${arr['en'][0]}</br>`);

*/

/*
//15
const arr = ['a', 'b', 'c', 'd']
document.write(`'${arr[0]} + ${arr[1]}, ${arr[2]} + ${arr[3]}'</br>`);

*/

/*
//16 + //17

const arr = new Array(parseInt(prompt('Введіть кількість елементів')))
for (let i = 0; i < arr.length; i++) {
    arr[i] =i;

    if (arr[i]==0) {
        document.write(`<div class='empty'>${arr[i]}</div></br>`);
    }
    else if (arr[i]%2==0){
    document.write(`<span class='even'>${arr[i]}</span></br>`);
    
    }else{
        document.write(`<p class='odd'>${arr[i]}</p></br>`);
    }
}
*/

//18

var vegetables = ['Капуста', 'Ріпа', 'Редиска', 'Морковка'];

str1 = vegetables.join(', ').toString();

document.write(str1); // "Капуста, Ріпа, Редиска, Морквина"


