import {fotos} from "./characters.js"

const mainContainer = document.querySelector(".mainContainer");
const containerLoader = document.querySelector(".loader_container");


function getServer(url, callback = () => { }) {
    containerLoader.classList.remove("hiden");
    const ajax = new XMLHttpRequest();
    ajax.open("get", url);
    ajax.send();
    ajax.addEventListener("readystatechange", () => {
        if (ajax.readyState === 4 && ajax.status >= 200 && ajax.status < 300) {
            callback(JSON.parse(ajax.response))
            containerLoader.classList.add("hiden");
        } else if (ajax.readyState === 4) {
            throw new Error(`Помилка у запиті на сервер : ${ajax.status} / ${ajax.statusText}`)
        }
    });
}

function showExchange(data = []) {
    const res = data.results;
    const resArr = res.map((elem,index)=>{    
        return {
            name : elem.name,
            height : elem.height,
            mass : elem.mass,
            hair_color : elem.hair_color,
            skin_color : elem.skin_color,
            eye_color : elem.eye_color,
            birth_year : elem.birth_year,
            gender : elem.gender,
            homeworld : elem.homeworld,
            foto : fotos[index].link,
        }
    });
    resArr.forEach((results) => {
        const info = `
        <div class="container">
            <div class="character"><img class="charImg" src="${results.foto}" alt="alt"></div>
                <ul class="cardInfo">
                <li class="name">name: ${results.name}</li>
                <li class="height">height: ${results.height}</li>
                <li class="mass">mass: ${results.mass}</li>
                <li class="hair">hair: ${results.hair_color}</li>
                <li class="skin">skin: ${results.skin_color}</li>
                <li class="eye">eye; ${results.eye_color}</li>
                <li class="birth">birth: ${results.birth_year}</li>
                <li class="gender">gender: ${results.gender}</li>
                <a href="${results.homeworld}"  target="_blank" >homeworld</a>
            </ul>
        </div>`;
        mainContainer.insertAdjacentHTML("afterbegin", info)
    })
}

getServer("https://swapi.dev/api/people",showExchange);