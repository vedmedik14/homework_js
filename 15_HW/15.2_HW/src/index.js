
const coments = document.querySelector(".coments");


function getCommentsArr(data = []) {
    const res = data.map((elem,index)=>{
        return {
            "id": index+1,
            "name": elem.name,
            "email":elem.email,
            "body":elem.body,
        }
    });
    
    return res;
}

async function getServer(url) {
    const respons = await fetch (url);
    const data = await respons.json();

    return getCommentsArr(data);
}

const arrList = await getServer("https://jsonplaceholder.typicode.com/comments");
let counter = 10;         
let currentPage = 0;

function displayList(comentsArr, plusRows, page) {
    const start = plusRows*page;
    const end = start + plusRows;
    
    const pagData = arrList.slice(start,end);

    pagData.forEach((results) => {
        const info = `
        <div class="comentContainer">
            <div class ="comentBody">
                <div class="comentName">
                    <div><h5>${results.name}</h5></div>
                    <div>id:${results.id}</div>
                </div>
                <div class="comentContent">
                    <span>${results.body}</span>
                </div>
                <div class="answer">
                    <a href="mailto:${results.email}">answer</a>
                </div>
            </div>
        </div>`;
        coments.insertAdjacentHTML("beforeend", info);
    });
    currentPage++;
}

displayList(arrList, counter, currentPage);

const loader = document.querySelector("#loader-bt");
loader.addEventListener("click",()=>{displayList(arrList, counter, currentPage)});