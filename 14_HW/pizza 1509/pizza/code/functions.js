import { pizzaSelectUser } from "./index.js";
import {pizza} from "./pizza.js";

function userSlectTopping(topping) {
    //size = "big"
    if ("smallmidbig".includes(topping)) {
        pizzaSelectUser.size = pizza.size.find((el) => {
            return el.name === topping
        })
    } else if ("moc1moc2moc3telyavetch1vetch2".includes(topping)) {
        pizzaSelectUser.topping.push(pizza.topping.find(el => el.name === topping))
    } else if ("sauceClassicsauceBBQsauceRikotta".includes(topping)) {
        pizzaSelectUser.sauce = pizza.sauce.find(el => el.name === topping)
    }
    pizzaSelectUser.price = show(pizzaSelectUser);
}

const endPrice = document.querySelector("#price");

function show(pizza) {
    let price = 0;
    if (pizza.sauce !== "") {
        price += pizza.sauce.price; 
    }
    if(pizza.topping.length > 0){
        price += pizza.topping.reduce((a,b)=>{
            return a + b.price
        }, 0)
    }
    if(pizza.size !== ""){
        price += pizza.size.price;
    }
    endPrice.innerText = price;
    console.log(price);
    return price;
}


const sauseWrap = document.querySelector("#sauce");
const toppingWrap = document.querySelector("#topping");

function selectedTopping(topping) {
    pizza.sauce.map((elem)=> {
        if (topping === elem.name) {
            const selectedSauce =`
                <div class="selectWraperSause">${elem.productName}</div>`;
            sauseWrap.insertAdjacentHTML("afterbegin",selectedSauce);
        }
    });

    pizza.topping.map((elem)=> {
        if (topping === elem.name) {
            const selectedTopping =`
                <div class="selectWraperTopping">${elem.productName}</div>`;
            toppingWrap.insertAdjacentHTML("afterbegin",selectedTopping);
        }        
    });  
}

    //baner
        function random(min, max) {
            const rand = min + Math.random() * (max - min + 1);
            return Math.floor(rand);
        }
    //

    //validator
        const validate = (p, v) => p.test(v);
    //

export { userSlectTopping,selectedTopping,random,validate}