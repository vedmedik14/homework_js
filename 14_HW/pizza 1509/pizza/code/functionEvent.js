import {userSlectTopping, selectedTopping, random, validate} from "./functions.js"
import { pizzaSelectUser } from "./index.js"
import {links} from "./pizza.js";

export function clickInputSize(e) {
    if(e.target.tagName === "INPUT"){
        userSlectTopping(e.target.value)
    }
}

// HW

    export const over = function(e) {
        e.preventDefault();
    }

    const table = document.querySelector(".table");
    const tblImg = document.querySelector("#tbl");   

    export const dragTable = (evt) =>{

        if (evt.preventDefault) evt.preventDefault();
        if (evt.stopPropagation) evt.stopPropagation();

        let id = evt.dataTransfer.getData("Text");
        tblImg.setAttribute("src",id);
 
        links.map((elem) => {
            if (id === elem.name) { 
                return id = elem.id;
            }
        });

        let elem = document.getElementById(id);

        if (elem.classList.contains("sauceImg")) {
            if (pizzaSelectUser.sauce ===  "") {
                userSlectTopping(id);   
                elem.classList.add("inv");
                selectedTopping(id);         
            }else{
                alert("Соус обрано");
            }
        }else if(elem.classList.contains("toppingImg")){
            if (pizzaSelectUser.topping.length<6) {
                userSlectTopping(id);
                selectedTopping(id);              
            }else{
                alert("Максимальна кількість");
            }
        }
    }

    const sause = document.querySelectorAll(".sauceImg");

     export function reselect(evt) {

        evt.target.remove();
        if (evt.target.className==="selectWraperSause") {
            pizzaSelectUser.sauce = "";
            sause.forEach((elem)=>{
                elem.classList.remove("inv");
            });
            
        }else if (evt.target.className==="selectWraperTopping") {
            pizzaSelectUser.topping = pizzaSelectUser.topping.filter(function(e) { return e.productName !== evt.target.innerText});
        }
     }



    //baner
        const but = document.querySelector("#banner");

        export const banerLeav = function() {
            but.classList.add("leav");
            but.classList.remove("fix");
            but.style.left = `${random(10,80)}%`;
            but.style.top = `${random(10,80)}%`;
        }
    //

    //validator
    export function validClientData(e) {
        if(e.target.dataset.type === "name" && validate(/^[а-яієґї'-]{2,}$/ig, e.target.value)){
            e.target.classList.add("valid")
        }else if(e.target.dataset.type === "phone" && validate(/^\+380\d{9}$/ig, e.target.value)){
            e.target.classList.add("valid");
        }else if(e.target.dataset.type === "email" && validate(/^[a-z_.0-9]+@[a-z0-9-.]+\.[a-z.]+$/ig, e.target.value)){
            e.target.classList.add("valid");
        }
    }

    export function cancelBt(){
        location.reload();
    }
    //
    
//

