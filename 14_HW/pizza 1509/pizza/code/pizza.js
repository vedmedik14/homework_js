const pizza = {
    size : [
        {name : "small", price : 40},
        {name : "mid", price: 55},
        {name : "big", price: 70}
    ],
    topping: [
        {name : "moc1",   price: 50, productName: "Сир звичайний"},
        {name : "moc2",   price: 55, productName: "Сир фета"},
        {name : "moc3",   price: 40, productName: "Моцарелла"},
        {name : "telya",  price: 80, productName: "Телятина"},
        {name : "vetch1", price: 30, productName: "Помiдори"},
        {name : "vetch2", price: 35, productName: "Гриби"},
    ],
    sauce: [
        {name: "sauceClassic", price : 20, productName: "Кетчуп" },
        {name: "sauceBBQ", price : 22, productName: "BBQ"},
        {name: "sauceRikotta", price : 24, productName: "Рiкотта"}
    ]
}

const links = [
    {name : "http://127.0.0.1:5500/pizza%201509/pizza/Pizza_pictures/sauceClassic.png", id: "sauceClassic"},
    {name : "http://127.0.0.1:5500/pizza%201509/pizza/Pizza_pictures/sous-bbq_155679418013.png", id: "sauceBBQ"},
    {name : "http://127.0.0.1:5500/pizza%201509/pizza/Pizza_pictures/sous-rikotta_1556623391103.png", id: "sauceRikotta"},

    {name : "http://127.0.0.1:5500/pizza%201509/pizza/Pizza_pictures/mocarela_1556623220308.png", id: "moc1"},
    {name : "http://127.0.0.1:5500/pizza%201509/pizza/Pizza_pictures/mocarela_1556785182818.png", id: "moc2"},
    {name : "http://127.0.0.1:5500/pizza%201509/pizza/Pizza_pictures/mocarela_1556785198489.png", id: "moc3"},
    {name : "http://127.0.0.1:5500/pizza%201509/pizza/Pizza_pictures/telyatina_1556624025747.png", id: "telya"},
    {name : "http://127.0.0.1:5500/pizza%201509/pizza/Pizza_pictures/vetchina.png", id: "vetch1"},
    {name : "http://127.0.0.1:5500/pizza%201509/pizza/Pizza_pictures/vetchina_1556623556129.png", id: "vetch2"}
]



export {pizza, links};